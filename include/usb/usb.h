/*
 * usb.h
 *
 *  Created on: May 8, 2014
 *      Author: Nikolay Filchenko
 */

#ifndef USB_H_
#define USB_H_

#include <vector>
#include <string>
#include <libusb.h>

#include "usb/usb_token.h"

class usb {
public:
	usb();
	~usb();
	std::vector<usb_token> tokens();
private:
	libusb_device **devs;
	int cnt;
};



#endif /* USB_H_ */

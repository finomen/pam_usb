/*
 * mass_Storage.h
 *
 *  Created on: May 10, 2014
 *      Author: finomen
 */

#ifndef MASS_STORAGE_H_
#define MASS_STORAGE_H_

#include <string>

class mass_storage {
public:
	mass_storage();
	~mass_storage();
	std::string read_mbr();

};



#endif /* MASS_STORAGE_H_ */

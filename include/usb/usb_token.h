/*
 * usb_token.h
 *
 *  Created on: Jun 12, 2014
 *      Author: finomen
 */

#ifndef USB_TOKEN_H_
#define USB_TOKEN_H_

#include <string>
#include <libusb.h>


class usb_token {
public:
	usb_token(libusb_device * dev);
	std::string get_serial() const;
	std::string get_name() const;
	std::string read_key() const;
	bool write_key(std::string const & key) const;
	bool is_mass_storage() const;
private:
	std::string serial;
	std::string name;
	mutable libusb_device_descriptor desc;
	libusb_device * dev;
};



#endif /* USB_TOKEN_H_ */

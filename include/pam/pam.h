/*
 * pam.h
 *
 *  Created on: May 8, 2014
 *      Author: Nikolay Filchenko
 */

#ifndef PAM_H_
#define PAM_H_

#include <security/pam_ext.h>
#include <string>

class pam {
private:
	typedef pam_handle * handle;
public:
	pam(pam_handle * ph);
	~pam();
	operator handle() const;
	std::string user() const;
	std::string get_item(int id) const;
	void set_item(int id, std::string const & value) const;
	std::string ask(std::string const & what, bool hide_input = false);
private:
	pam_handle * ph;
};

pam * pam_context(pam_handle * ph = 0, bool remove = false);

class pam_guard {
public:
	pam_guard(pam_handle * ph);
	~pam_guard();
};

#endif /* PAM_H_ */

/*
 * log.h
 *
 *  Created on: May 8, 2014
 *      Author: Nikolay Filchenko
 */

#ifndef H_LOG
#define H_LOG

#include <sstream>
#include <utility>

#include <syslog.h>
#include <security/pam_ext.h>
#include <iostream>
#include <memory>
#include "pam/pam.h"

namespace logger {

	template<int category>
	class syslog_wrapper {
	private:
		class syslog_proxy {
		public:
			syslog_proxy() : stream(std::make_shared<std::ostringstream>()) {
			}

			syslog_proxy(syslog_proxy const &) = delete;
			syslog_proxy(syslog_proxy&& prev) : stream(prev.stream) {
				prev.stream = 0;
			}
		public:
			template<typename T>
			syslog_proxy & operator <<(T const & data) {
				(*stream) << data;
				return *this;
			}

			~syslog_proxy() {
				if (!stream) {
					return;
				}

				std::cout << "[" << category << "] " << stream->str() << std::endl;
				pam_syslog(*pam_context(), category, "%s", stream->str().c_str());
			}
		private:
			std::shared_ptr<std::ostringstream> stream;
		};

	public:
		template<typename T>
		syslog_proxy operator <<(T const & data) {
			syslog_proxy proxy;
			proxy << data;
			return std::move(proxy);
		}

	private:
		pam_handle_t * ph;
	};

	extern syslog_wrapper<LOG_ERR> error;
	extern syslog_wrapper<LOG_AUTH> auth;
	extern syslog_wrapper<LOG_DEBUG> debug;

}

#endif //H_LOG

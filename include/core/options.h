/*
 * options.h
 *
 *  Created on: Jun 12, 2014
 *      Author: finomen
 */

#ifndef OPTIONS_H_
#define OPTIONS_H_

#include <string>
#include <map>

class options {
public:
	options(int argc, const char * argv[]);
	std::string get(std::string const & name, std::string const & def);
	size_t get(std::string const & name, size_t def);
	bool has_flag(std::string const & flag);
private:
	std::map<std::string, std::string> args;
};



#endif /* OPTIONS_H_ */

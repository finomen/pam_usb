/*
 * key.h
 *
 *  Created on: Jun 11, 2014
 *      Author: finomen
 */

#ifndef KEY_H_
#define KEY_H_

#include <string>

std::string gen_key(bool strong);

#endif /* KEY_H_ */

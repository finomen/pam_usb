/*
 * store.h
 *
 *  Created on: Jun 11, 2014
 *      Author: finomen
 */

#ifndef STORE_H_
#define STORE_H_

#include <string>

class store {
public:
	static inline store & instance() {
		static store i;
		return i;
	}
	std::string get_serial(std::string const & username);
	void set_serial(std::string const & username, std::string const & serial);
	size_t get_retries(std::string const & username);
	void reset_retries(std::string const & username);
	void increment_retries(std::string const & username);
private:
	store();
	store(store &) = delete;
	store(store &&) = delete;
};




#endif /* STORE_H_ */

/*
 * crypt.h
 *
 *  Created on: Jun 11, 2014
 *      Author: finomen
 */

#ifndef AES_H_
#define AES_H_

#include <string>
#include <openssl/evp.h>


class aes {
public:
	aes(std::string const & passpharse);
	~aes();
	std::string encrypt(std::string const & data) const;
	std::string decrypt(std::string const & data) const;
private:
	mutable EVP_CIPHER_CTX enc;
	mutable EVP_CIPHER_CTX dec;
};



#endif /* CRYPT_H_ */

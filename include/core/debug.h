/*
 * debug.h
 *
 *  Created on: May 8, 2014
 *      Author: finomen
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#include "core/log.h"

#ifdef DEBUG
#define FUNCTION_ENTER logger::debug << "Enter " << __FUNCTION__;
#define FUNCTION_RETURN logger::debug << "Return " << __FUNCTION__;
#else
#define FUNCTION_ENTER
#define FUNCTION_RETURN
#endif



#endif /* DEBUG_H_ */

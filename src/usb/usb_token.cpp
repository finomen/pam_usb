/*
 * usb_token.cpp
 *
 *  Created on: Jun 12, 2014
 *      Author: Nikolay Filchenko
 */

#include "usb/usb_token.h"
#include "core/log.h"
#include "core/debug.h"

#include <sstream>

std::string read_mbr(libusb_device_handle * handle,
		libusb_interface_descriptor const & interface);
bool write_mbr(libusb_device_handle * handle,
		libusb_interface_descriptor const & interface, std::string const & data);

namespace {

std::string get_string(uint8_t id, libusb_device * dev) {
	FUNCTION_ENTER
	unsigned char buf[100];
	libusb_device_handle * handle;
	libusb_open(dev, &handle);

	auto r = libusb_get_string_descriptor(handle, id, 0, buf,
			100);

	libusb_close(handle);
	if (r <= 0) {
		logger::error << "Get string fail";
		FUNCTION_RETURN
		return "";
	}

	std::string  res;
	for (size_t i = 0; i < buf[0]; i += 2) {
		res += buf[i];
	}

	FUNCTION_RETURN
	return res;
}



std::pair<int, int> get_mass_storage_interface(libusb_config_descriptor *conf_desc) {
	FUNCTION_ENTER
	auto nb_ifaces = conf_desc->bNumInterfaces;
	for (int i = 0; i < nb_ifaces; i++) {
		for (int j = 0; j < conf_desc->interface[i].num_altsetting;
				j++) {
			if ((conf_desc->interface[i].altsetting[j].bInterfaceClass
					== LIBUSB_CLASS_MASS_STORAGE)
					&& ((conf_desc->interface[i].altsetting[j].bInterfaceSubClass
							== 0x01)
							|| (conf_desc->interface[i].altsetting[j].bInterfaceSubClass
									== 0x06))
					&& (conf_desc->interface[i].altsetting[j].bInterfaceProtocol
							== 0x50)) {
				FUNCTION_RETURN
				return std::make_pair(i, j);
			}
		}
	}

	FUNCTION_RETURN
	return std::make_pair(-1, -1);
}
}

usb_token::usb_token(libusb_device * dev) : dev(dev) {
	FUNCTION_ENTER
	int r = libusb_get_device_descriptor(dev, &desc);

	if (r < 0) {
		logger::error << "failed to get device descriptor";
	}

	FUNCTION_RETURN
}

std::string usb_token::get_serial() const {
	FUNCTION_ENTER
	if (desc.iSerialNumber != 0) {
		FUNCTION_RETURN
		return get_string(desc.iSerialNumber, dev);
	}

	FUNCTION_RETURN
	return "";
}

std::string usb_token::get_name() const {
	FUNCTION_ENTER
	std::ostringstream name;
	if (desc.iManufacturer != 0) {
		FUNCTION_RETURN
		name << get_string(desc.iManufacturer, dev) << " ";
	}

	if (desc.iProduct != 0) {
		FUNCTION_RETURN
		name << get_string(desc.iProduct, dev) << " ";
	}

	name << "[" << get_serial() << "]";

	FUNCTION_RETURN
	return name.str();
}

std::string usb_token::read_key() const {
	FUNCTION_ENTER
	libusb_config_descriptor *conf_desc;
	if (libusb_get_config_descriptor(dev, 0, &conf_desc) < 0) {
		FUNCTION_RETURN
		return "";
	}

	libusb_device_handle * handle;
	libusb_open(dev, &handle);
	libusb_set_auto_detach_kernel_driver(handle, 1);

	auto mi = get_mass_storage_interface(conf_desc);

	if (mi.first == -1) {
		libusb_free_config_descriptor(conf_desc);
		libusb_close(handle);
		FUNCTION_RETURN
		return "";
	}

	auto key = std::string();
	auto r = libusb_claim_interface(handle, mi.first);
	if (r == LIBUSB_SUCCESS) {
		key = read_mbr(handle,
						conf_desc->interface[mi.first].altsetting[mi.second]);
	}

	libusb_release_interface(handle, mi.first);

	libusb_free_config_descriptor(conf_desc);
	libusb_close(handle);
	FUNCTION_RETURN
	return key;
}

bool usb_token::write_key(std::string const & key) const {
	FUNCTION_ENTER

	libusb_config_descriptor *conf_desc;
	if (libusb_get_config_descriptor(dev, 0, &conf_desc) < 0) {
		FUNCTION_RETURN
		return false;
	}

	libusb_device_handle * handle;
	libusb_open(dev, &handle);
	libusb_set_auto_detach_kernel_driver(handle, 1);

	auto mi = get_mass_storage_interface(conf_desc);

	if (mi.first == -1) {
		libusb_free_config_descriptor(conf_desc);
		libusb_close(handle);
		FUNCTION_RETURN
		return false;
	}

	auto r = libusb_claim_interface(handle, mi.first);
	auto res = false;
	if (r == LIBUSB_SUCCESS) {
		res = write_mbr(handle,
			conf_desc->interface[mi.first].altsetting[mi.second], key);
	}

	libusb_release_interface(handle, mi.first);

	libusb_free_config_descriptor(conf_desc);
	libusb_close(handle);
	FUNCTION_RETURN
	return res;
}

bool usb_token::is_mass_storage() const {
	FUNCTION_ENTER
	libusb_config_descriptor *conf_desc;
	if (libusb_get_config_descriptor(dev, 0, &conf_desc) < 0) {
		FUNCTION_RETURN
		return false;
	}

	libusb_device_handle * handle;
	libusb_open(dev, &handle);
	libusb_set_auto_detach_kernel_driver(handle, 1);

	auto mi = get_mass_storage_interface(conf_desc);

	libusb_free_config_descriptor(conf_desc);
	libusb_close(handle);
	FUNCTION_RETURN
	return mi.first >= 0;
}




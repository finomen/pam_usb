/*
 * mass_storage.cpp
 *
 *  Created on: May 10, 2014
 *      Author: finomen
 */
#include <libusb.h>
#include <cstring>
#include "usb/mass_storage.h"
#include <iostream>

#define BOMS_RESET                    0xFF
#define BOMS_GET_MAX_LUN              0xFE
#define RETRY_MAX                     5
#define REQUEST_SENSE_LENGTH          0x12
#define INQUIRY_LENGTH                0x24
#define READ_CAPACITY_LENGTH          0x08

#define be_to_int32(buf) (((buf)[0]<<24)|((buf)[1]<<16)|((buf)[2]<<8)|(buf)[3])

// Section 5.1: Command Block Wrapper (CBW)
struct command_block_wrapper {
	uint8_t dCBWSignature[4];
	uint32_t dCBWTag;
	uint32_t dCBWDataTransferLength;
	uint8_t bmCBWFlags;
	uint8_t bCBWLUN;
	uint8_t bCBWCBLength;
	uint8_t CBWCB[16];
};

// Section 5.2: Command Status Wrapper (CSW)
struct command_status_wrapper {
	uint8_t dCSWSignature[4];
	uint32_t dCSWTag;
	uint32_t dCSWDataResidue;
	uint8_t bCSWStatus;
};

static uint8_t cdb_length[256] = {
//	 0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
		06, 06, 06, 06, 06, 06, 06, 06, 06, 06, 06, 06, 06, 06, 06, 06,  //  0
		06, 06, 06, 06, 06, 06, 06, 06, 06, 06, 06, 06, 06, 06, 06, 06,  //  1
		10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,  //  2
		10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,  //  3
		10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,  //  4
		10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10,  //  5
		00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,  //  6
		00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,  //  7
		16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,  //  8
		16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,  //  9
		12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12,  //  A
		12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12,  //  B
		00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,  //  C
		00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,  //  D
		00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,  //  E
		00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,  //  F
		};

static int send_mass_storage_command(libusb_device_handle *handle,
		uint8_t endpoint, uint8_t lun, uint8_t *cdb, uint8_t direction,
		int data_length, uint32_t *ret_tag) {
	static uint32_t tag = 1;
	uint8_t cdb_len;
	int i, r, size;
	struct command_block_wrapper cbw;

	if (cdb == NULL) {
		return -1;
	}

	if (endpoint & LIBUSB_ENDPOINT_IN) {
		return -1;
	}

	cdb_len = cdb_length[cdb[0]];
	if ((cdb_len == 0) || (cdb_len > sizeof(cbw.CBWCB))) {
		return -1;
	}

	memset(&cbw, 0, sizeof(cbw));
	cbw.dCBWSignature[0] = 'U';
	cbw.dCBWSignature[1] = 'S';
	cbw.dCBWSignature[2] = 'B';
	cbw.dCBWSignature[3] = 'C';
	*ret_tag = tag;
	cbw.dCBWTag = tag++;
	cbw.dCBWDataTransferLength = data_length;
	cbw.bmCBWFlags = direction;
	cbw.bCBWLUN = lun;
	// Subclass is 1 or 6 => cdb_len
	cbw.bCBWCBLength = cdb_len;
	memcpy(cbw.CBWCB, cdb, cdb_len);

	i = 0;
	do {
		// The transfer length must always be exactly 31 bytes.
		r = libusb_bulk_transfer(handle, endpoint, (unsigned char*) &cbw, 31,
				&size, 1000);
		if (r == LIBUSB_ERROR_PIPE) {
			libusb_clear_halt(handle, endpoint);
		}
		i++;
	} while ((r == LIBUSB_ERROR_PIPE) && (i < RETRY_MAX));
	if (r != LIBUSB_SUCCESS) {
		return -1;
	}

	return 0;
}

static int get_mass_storage_status(libusb_device_handle *handle,
		uint8_t endpoint, uint32_t expected_tag) {
	int i, r, size;
	struct command_status_wrapper csw;

	i = 0;
	do {
		r = libusb_bulk_transfer(handle, endpoint, (unsigned char*) &csw, 13,
				&size, 1000);
		if (r == LIBUSB_ERROR_PIPE) {
			libusb_clear_halt(handle, endpoint);
		}
		i++;
	} while ((r == LIBUSB_ERROR_PIPE) && (i < RETRY_MAX));

	if (r != LIBUSB_SUCCESS) {
		return -1;
	}

	if (size != 13) {
		return -1;
	}

	if (csw.dCSWTag != expected_tag) {
		return -1;
	}

	if (csw.dCSWTag != expected_tag)
		return -1;

	if (csw.bCSWStatus) {
		if (csw.bCSWStatus == 1)
			return -2;
		else
			return -1;
	}

	return 0;
}

static void get_sense(libusb_device_handle *handle, uint8_t endpoint_in,
		uint8_t endpoint_out) {
	uint8_t cdb[16];	// SCSI Command Descriptor Block
	uint8_t sense[18];
	uint32_t expected_tag;
	int size;
	int rc;

	// Request Sense
	memset(sense, 0, sizeof(sense));
	memset(cdb, 0, sizeof(cdb));
	cdb[0] = 0x03;	// Request Sense
	cdb[4] = REQUEST_SENSE_LENGTH;

	send_mass_storage_command(handle, endpoint_out, 0, cdb, LIBUSB_ENDPOINT_IN,
	REQUEST_SENSE_LENGTH, &expected_tag);
	rc = libusb_bulk_transfer(handle, endpoint_in, (unsigned char*) &sense,
	REQUEST_SENSE_LENGTH, &size, 1000);
	if (rc < 0) {
		return;
	}

	get_mass_storage_status(handle, endpoint_in, expected_tag);
}

std::string read_mbr(libusb_device_handle * handle,
		libusb_interface_descriptor const & interface) {

	const struct libusb_endpoint_descriptor *endpoint;
	uint8_t endpoint_in = 0, endpoint_out = 0;

	for (int k = 0; k < interface.bNumEndpoints; k++) {
		struct libusb_ss_endpoint_companion_descriptor *ep_comp = NULL;
		endpoint = &interface.endpoint[k];

		// Use the first interrupt or bulk IN/OUT endpoints as default for testing
		if ((endpoint->bmAttributes & LIBUSB_TRANSFER_TYPE_MASK)
				& (LIBUSB_TRANSFER_TYPE_BULK | LIBUSB_TRANSFER_TYPE_INTERRUPT)) {
			if (endpoint->bEndpointAddress & LIBUSB_ENDPOINT_IN) {
				if (!endpoint_in)
					endpoint_in = endpoint->bEndpointAddress;
			} else {
				if (!endpoint_out)
					endpoint_out = endpoint->bEndpointAddress;
			}
		}

		libusb_get_ss_endpoint_companion_descriptor(NULL, endpoint, &ep_comp);
		if (ep_comp) {
			libusb_free_ss_endpoint_companion_descriptor(ep_comp);
		}
	}

	int r, size;
	uint8_t lun;
	uint32_t expected_tag;
	uint32_t block_size;
	uint8_t cdb[16];	// SCSI Command Descriptor Block
	uint8_t buffer[64];
	unsigned char *data;

	r = libusb_control_transfer(handle,
			LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_CLASS
					| LIBUSB_RECIPIENT_INTERFACE, BOMS_GET_MAX_LUN, 0, 0, &lun,
			1, 1000);
	// Some devices send a STALL instead of the actual value.
	// In such cases we should set lun to 0.
	if (r == 0) {
		lun = 0;
	} else if (r < 0) {
		return "";
	}

	memset(buffer, 0, sizeof(buffer));
	memset(cdb, 0, sizeof(cdb));
	cdb[0] = 0x12;	// Inquiry
	cdb[4] = INQUIRY_LENGTH;

	// Read capacity
	memset(buffer, 0, sizeof(buffer));
	memset(cdb, 0, sizeof(cdb));
	cdb[0] = 0x25;	// Read Capacity

	send_mass_storage_command(handle, endpoint_out, lun, cdb,
			LIBUSB_ENDPOINT_IN, READ_CAPACITY_LENGTH, &expected_tag);

	libusb_bulk_transfer(handle, endpoint_in, (unsigned char*) &buffer,
	READ_CAPACITY_LENGTH, &size, 1000); //TODO: check return
	block_size = be_to_int32(&buffer[4]);

	if (get_mass_storage_status(handle, endpoint_in, expected_tag) == -2) {
		get_sense(handle, endpoint_in, endpoint_out);
	}

	data = (unsigned char*) calloc(1, block_size);
	if (data == NULL) {
		return "";
	}

	memset(cdb, 0, sizeof(cdb));

	cdb[0] = 0x28;	// Read(10)
	cdb[8] = 0x01;	// 1 block

	send_mass_storage_command(handle, endpoint_out, lun, cdb,
			LIBUSB_ENDPOINT_IN, block_size, &expected_tag);
	libusb_bulk_transfer(handle, endpoint_in, data, block_size, &size, 5000);
	size = std::min(size, (int)data[0]);
	auto res = std::string((char*)data + 1, size);

	if (get_mass_storage_status(handle, endpoint_in, expected_tag) == -2) {
		get_sense(handle, endpoint_in, endpoint_out);
	}

	free(data);
	return res;
}


bool write_mbr(libusb_device_handle * handle,
		libusb_interface_descriptor const & interface, std::string const & key) {

	const struct libusb_endpoint_descriptor *endpoint;
	uint8_t endpoint_in = 0, endpoint_out = 0;

	for (int k = 0; k < interface.bNumEndpoints; k++) {
		struct libusb_ss_endpoint_companion_descriptor *ep_comp = NULL;
		endpoint = &interface.endpoint[k];

		// Use the first interrupt or bulk IN/OUT endpoints as default for testing
		if ((endpoint->bmAttributes & LIBUSB_TRANSFER_TYPE_MASK)
				& (LIBUSB_TRANSFER_TYPE_BULK | LIBUSB_TRANSFER_TYPE_INTERRUPT)) {
			if (endpoint->bEndpointAddress & LIBUSB_ENDPOINT_IN) {
				if (!endpoint_in)
					endpoint_in = endpoint->bEndpointAddress;
			} else {
				if (!endpoint_out)
					endpoint_out = endpoint->bEndpointAddress;
			}
		}

		libusb_get_ss_endpoint_companion_descriptor(NULL, endpoint, &ep_comp);
		if (ep_comp) {
			libusb_free_ss_endpoint_companion_descriptor(ep_comp);
		}
	}

	int r, size;
	uint8_t lun;
	uint32_t expected_tag;
	uint32_t block_size;
	uint8_t cdb[16];	// SCSI Command Descriptor Block
	uint8_t buffer[64];
	unsigned char *data;

	r = libusb_control_transfer(handle,
			LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_CLASS
					| LIBUSB_RECIPIENT_INTERFACE, BOMS_GET_MAX_LUN, 0, 0, &lun,
			1, 1000);
	// Some devices send a STALL instead of the actual value.
	// In such cases we should set lun to 0.
	if (r == 0) {
		lun = 0;
	} else if (r < 0) {
		return false;
	}

	memset(buffer, 0, sizeof(buffer));
	memset(cdb, 0, sizeof(cdb));
	cdb[0] = 0x12;	// Inquiry
	cdb[4] = INQUIRY_LENGTH;

	// Read capacity
	memset(buffer, 0, sizeof(buffer));
	memset(cdb, 0, sizeof(cdb));
	cdb[0] = 0x25;	// Read Capacity

	send_mass_storage_command(handle, endpoint_out, lun, cdb,
			LIBUSB_ENDPOINT_IN, READ_CAPACITY_LENGTH, &expected_tag);

	libusb_bulk_transfer(handle, endpoint_in, (unsigned char*) &buffer,
	READ_CAPACITY_LENGTH, &size, 1000); //TODO: check return
	block_size = be_to_int32(&buffer[4]);

	if (get_mass_storage_status(handle, endpoint_in, expected_tag) == -2) {
		get_sense(handle, endpoint_in, endpoint_out);
	}

	data = (unsigned char*) calloc(1, block_size);
	if (data == 0) {
		return false;
	}

	memset(cdb, 0, sizeof(cdb));

	cdb[0] = 0x2A;	// Write(10)
	cdb[8] = 0x01;	// 1 block

	data[0] = (unsigned char)key.length();
	memcpy(data + 1, key.c_str(), key.length());

	std::cout <<"KEY SIZE " << key.length() << std::endl;

	send_mass_storage_command(handle, endpoint_out, lun, cdb,
			LIBUSB_ENDPOINT_OUT, block_size, &expected_tag);



	libusb_bulk_transfer(handle, endpoint_out, data, key.length() + 1, &size, 5000);


	if (get_mass_storage_status(handle, endpoint_in, expected_tag) == -2) {
		get_sense(handle, endpoint_in, endpoint_out);
	}

	free(data);
	return true;
}

/*
 * usb.cpp
 *
 *  Created on: May 8, 2014
 *      Author: Nikolay Filchenko
 */

#include "core/log.h"
#include "core/debug.h"
#include "usb/usb.h"
#include <libusb.h>


usb::usb() : devs(0), cnt(0) {
	FUNCTION_ENTER

	auto r = libusb_init(NULL);
	if (r < 0) {
		FUNCTION_RETURN
		return;
	}

	cnt = libusb_get_device_list(0, &devs);
	if (cnt < 0) {
		FUNCTION_RETURN
		return;
	}

	FUNCTION_RETURN
}

usb::~usb() {
	FUNCTION_ENTER
	if (devs == 0) {
		FUNCTION_RETURN
		return;
	}

	libusb_free_device_list(devs, 1);
	libusb_exit(0);
	FUNCTION_RETURN
}

std::vector<usb_token> usb::tokens() {
	FUNCTION_ENTER
	libusb_device *dev;
	int i = 0;

	std::vector<usb_token> res;
	if (devs == 0) {
		FUNCTION_RETURN
		return res;
	}

	while ((dev = devs[i++]) != NULL) {
		usb_token token(dev);
		if (token.is_mass_storage()) {
			res.push_back(token);
		}
	}

	FUNCTION_RETURN
	return res;
}

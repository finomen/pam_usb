set(SOURCES
    log.cpp
    aes.cpp
    options.cpp
    store.cpp
    key.cpp
)

add_library(core ${SOURCES})

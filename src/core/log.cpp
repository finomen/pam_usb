/*
 * log.cpp
 *
 *  Created on: May 8, 2014
 *      Author: Nikolay Filchenko
 */

#include "core/log.h"

namespace logger {
	syslog_wrapper<LOG_AUTH> auth;
	syslog_wrapper<LOG_ERR> error;
	syslog_wrapper<LOG_DEBUG> debug;
}

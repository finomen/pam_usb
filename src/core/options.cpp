/*
 * options.cpp
 *
 *  Created on: Jun 12, 2014
 *      Author: Nikolay Filchenko
 */

#include <cstdlib>
#include "core/options.h"

options::options(int argc, const char * argv[]) {
	for (int i = 0; i < argc; ++i) {
		auto arg = std::string(argv[i]);
		auto split = arg.find_first_of('=');
		if (split != std::string::npos) {
			args[arg.substr(0, split)] = arg.substr(split + 1);
		} else {
			args[arg.substr(0, split)] = "";
		}
	}
}

std::string options::get(std::string const & name, std::string const & def) {
	if (args.count(name) != 0) {
			return args[name];
		}

		return def;
}

size_t options::get(std::string const & name, size_t def) {
	if (args.count(name) != 0) {
		return atoi(args[name].c_str());
	}

	return def;
}

bool options::has_flag(std::string const & flag) {
	return args.count(flag) != 0;
}




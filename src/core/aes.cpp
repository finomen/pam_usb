/*
 * aes.cpp
 *
 *  Created on: Jun 11, 2014
 *      Author: Nikolay Filchenko
 */

#include <stdexcept>

#include "core/aes.h"
#include "core/log.h"

namespace {
enum {
	SHA_ROUNDS = 5, AES_BLOCK_SIZE = 256, AES_KEY_SIZE = AES_BLOCK_SIZE >> 3

};

unsigned char salt[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
}

aes::aes(std::string const & passpharse) {
	unsigned char key[AES_KEY_SIZE], iv[AES_KEY_SIZE];

	auto i = EVP_BytesToKey(EVP_aes_256_cbc(), EVP_sha1(), salt,
			reinterpret_cast<const unsigned char *>(passpharse.c_str()),
			passpharse.length(), SHA_ROUNDS, key, iv);

	if (i != AES_KEY_SIZE) {
		logger::error << "Failed to generate AES key, actual size is " << (i << 3)
				<< " bits, expected " << AES_BLOCK_SIZE << " bits";
		throw std::logic_error("Failed to generate AES key");
	}

	EVP_CIPHER_CTX_init(&enc);
	EVP_EncryptInit_ex(&enc, EVP_aes_256_cbc(), NULL, key, iv);
	EVP_CIPHER_CTX_init(&dec);
	EVP_DecryptInit_ex(&dec, EVP_aes_256_cbc(), NULL, key, iv);
}

aes::~aes() {
	EVP_CIPHER_CTX_cleanup(&dec);
	EVP_CIPHER_CTX_cleanup(&enc);
}

std::string aes::encrypt(std::string const & data) const {
	int p_len = data.length(), f_len = 0;

	if (!EVP_EncryptInit_ex(&enc, 0, 0, 0, 0)) {
		return "";
	}

	auto plaintext = new unsigned char[data.length() + AES_BLOCK_SIZE - 1];

	if (!EVP_EncryptUpdate(&enc, plaintext, &p_len,
			reinterpret_cast<const unsigned char *>(data.c_str()),
			data.length())) {

		free(plaintext);
		return "";
	}

	if (!EVP_EncryptFinal_ex(&enc, plaintext + p_len, &f_len)) {
		free(plaintext);
		return "";
	}

	auto res = std::string(reinterpret_cast<char*>(plaintext), p_len + f_len);

	free(plaintext);

	return res;
}

std::string aes::decrypt(std::string const & data) const {
	int p_len = data.length(), f_len = 0;

	if (!EVP_DecryptInit_ex(&dec, 0, 0, 0, 0)) {
		return "";
	}

	auto plaintext = new unsigned char[data.length()];

	if (!EVP_DecryptUpdate(&dec, plaintext, &p_len,
			reinterpret_cast<const unsigned char *>(data.c_str()),
			data.length())) {

		free(plaintext);
		return "";
	}

	if (!EVP_DecryptFinal_ex(&dec, plaintext + p_len, &f_len)) {
		free(plaintext);
		return "";
	}

	auto res = std::string(reinterpret_cast<char*>(plaintext), p_len + f_len);

	free(plaintext);

	return res;
}

/*
 * key.cpp
 *
 *  Created on: Jun 12, 2014
 *      Author: Nikolay Filchenko
 */

#include "core/key.h"
#include "core/debug.h"
#include <string>
#include <cstdio>

namespace {
	enum {
		KEY_LEN = 256 >> 3
	};
}

std::string gen_key(bool strong) {
	FUNCTION_ENTER
	std::string key;
	auto file = fopen(strong ? "/dev/random" : "/dev/urandom", "rb");

	while (key.length() < KEY_LEN) {
		char c;
		if (fread(&c, 1, 1, file) == 1) {
			key += c;
		}
	}

	fclose(file);
	FUNCTION_RETURN
	return key;
}




/*
 * crypt.cpp
 *
 *  Created on: Jun 9, 2014
 *      Author: finomen
 */

#include <string>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <openssl/evp.h>
#include <iostream>

#define AES_BLOCK_SIZE 256

/**
 * Create an 256 bit key and IV using the supplied key_data. salt can be added for taste.
 * Fills in the encryption and decryption ctx objects and returns 0 on success
 **/
int aes_init(unsigned char *key_data, int key_data_len, unsigned char *salt,
		EVP_CIPHER_CTX *e_ctx, EVP_CIPHER_CTX *d_ctx) {
	int i, nrounds = 5;
	unsigned char key[32], iv[32];

	/*
	 * Gen key & IV for AES 256 CBC mode. A SHA1 digest is used to hash the supplied key material.
	 * nrounds is the number of times the we hash the material. More rounds are more secure but
	 * slower.
	 */
	i = EVP_BytesToKey(EVP_aes_256_cbc(), EVP_sha1(), salt, key_data,
			key_data_len, nrounds, key, iv);
	if (i != 32) {
		printf("Key size is %d bits - should be 256 bits\n", i);
		return -1;
	}

	EVP_CIPHER_CTX_init(e_ctx);
	EVP_EncryptInit_ex(e_ctx, EVP_aes_256_cbc(), NULL, key, iv);
	EVP_CIPHER_CTX_init(d_ctx);
	EVP_DecryptInit_ex(d_ctx, EVP_aes_256_cbc(), NULL, key, iv);

	return 0;
}

/*
 * Encrypt *len bytes of data
 * All data going in & out is considered binary (unsigned char[])
 */
unsigned char *aes_encrypt(EVP_CIPHER_CTX *e, const unsigned char *plaintext,
		int *len) {
	/* max ciphertext len for a n bytes of plaintext is n + AES_BLOCK_SIZE -1 bytes */
	int c_len = *len + AES_BLOCK_SIZE - 1, f_len = 0;
	printf("Allocate %d->%d\n", *len, c_len);
	unsigned char *ciphertext = (unsigned char *) malloc(c_len);

	printf("Init\n");
	/* allows reusing of 'e' for multiple encryption cycles */
	if (!EVP_EncryptInit_ex(e, NULL, NULL, NULL, NULL)) {
		printf("ERROR in EVP_EncryptInit_ex \n");
		return NULL;
	}

	printf("Update\n");
	/* update ciphertext, c_len is filled with the length of ciphertext generated,
	 *len is the size of plaintext in bytes */
	if (!EVP_EncryptUpdate(e, ciphertext, &c_len, plaintext, *len)) {
		printf("ERROR in EVP_EncryptUpdate \n");
		return NULL;
	}

	printf("Final\n");
	/* update ciphertext with the final remaining bytes */
	if (!EVP_EncryptFinal_ex(e, ciphertext + c_len, &f_len)) {
		printf("ERROR in EVP_EncryptFinal_ex \n");
		return NULL;
	}

	*len = c_len + f_len;
	return ciphertext;
}

/*
 * Decrypt *len bytes of ciphertext
 */
unsigned char *aes_decrypt(EVP_CIPHER_CTX *e, const unsigned char *ciphertext,
		int *len) {
	/* plaintext will always be equal to or lesser than length of ciphertext*/
	int p_len = *len, f_len = 0;
	unsigned char *plaintext = (unsigned char *) malloc(p_len);
	std::cout << "DS" << std::endl;

	if (!EVP_DecryptInit_ex(e, NULL, NULL, NULL, NULL)) {
		printf("ERROR in EVP_DecryptInit_ex \n");
		return NULL;
	}

	if (!EVP_DecryptUpdate(e, plaintext, &p_len, ciphertext, *len)) {
		printf("ERROR in EVP_DecryptUpdate\n");
		return NULL;
	}

	if (!EVP_DecryptFinal_ex(e, plaintext + p_len, &f_len)) {
		printf("ERROR in EVP_DecryptFinal_ex\n");
		return NULL;
	}

	*len = p_len + f_len;
	return plaintext;
}

std::string decrypt_key(std::string const & data, std::string const & key) {
	EVP_CIPHER_CTX en, de;
	unsigned char salt[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
	unsigned char *key_data;
	int key_data_len;


	key_data = (unsigned char*) key.c_str();
	key_data_len = key.length();


	if (aes_init(key_data, key_data_len, salt, &en, &de)) {
		printf("Couldn't initialize AES cipher\n");
		return "";
	}

	int len = data.length();
	auto plaintext = (char *)aes_decrypt(&de, (const unsigned char *)data.c_str(), &len);
	//auto plaintext = (char *) aes_encrypt(&en,(const unsigned char *) data.c_str(), &len);
	auto res = plaintext ? std::string(plaintext, len) : "";

	free(plaintext);
	/*plaintext = (char *) aes_decrypt(&de,
				(const unsigned char *) res.c_str(), &len);
	std::cout << "Dec:" << plaintext << std::endl;
	free(plaintext);*/

	EVP_CIPHER_CTX_cleanup(&de);
	EVP_CIPHER_CTX_cleanup(&en);
	return res;
}

std::string encrypt_key(std::string const & data, std::string const & key) {
	EVP_CIPHER_CTX en, de;
	unsigned char salt[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
	unsigned char *key_data;
	int key_data_len;


	key_data = (unsigned char*) key.c_str();
	key_data_len = key.length();


	if (aes_init(key_data, key_data_len, salt, &en, &de)) {
		printf("Couldn't initialize AES cipher\n");
		return "";
	}

	int len = data.length();
	//auto plaintext = (char *)aes_decrypt(&de, (const unsigned char *)data.c_str(), &len);
	auto plaintext = (char *) aes_encrypt(&en,(const unsigned char *) data.c_str(), &len);
	auto res = plaintext ? std::string(plaintext, len) : "";

	free(plaintext);
	/*plaintext = (char *) aes_decrypt(&de,
				(const unsigned char *) res.c_str(), &len);
	std::cout << "Dec:" << plaintext << std::endl;
	free(plaintext);*/

	EVP_CIPHER_CTX_cleanup(&de);
	EVP_CIPHER_CTX_cleanup(&en);
	return res;
}

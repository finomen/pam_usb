/*
 * store.cpp
 *
 *  Created on: Jun 12, 2014
 *      Author: Nikolay Filchenko
 */

#include "core/store.h"
#include "core/debug.h"
#include <fstream>
#include <algorithm>

#include <unistd.h>

namespace {
	auto filename = "/etc/tokens";
}

store::store() {

}

std::string store::get_serial(std::string const & username) {
	FUNCTION_ENTER
	std::ifstream in(filename, std::ios::in);
	while (in.good()) {
		std::string name, serial;
		int retries;
		in >> name >> serial >> retries;
		if (name == username) {
			in.close();
			FUNCTION_RETURN
			return serial;
		}
	}

	in.close();
	FUNCTION_RETURN
	return "";
}

void store::set_serial(std::string const & username, std::string const & serial_) {
	FUNCTION_ENTER
	std::ifstream in(filename, std::ios::in);
	auto file = "/etc/.tokens";//tmpnam(0);
	std::ofstream out(file, std::ios::out);
	bool done = false;
	while (in.good()) {
		std::string name, serial;
		int retries;
		in >> name >> serial >> retries;
		if (name.length() == 0) {
			continue;
		}
		if (name == username) {
			serial = serial_;
			done = true;
		}

		out << name << " " << serial << " " << retries << "\n";
	}

	if (!done) {
		out << username << " " << serial_ << " " << 0;
	}

	in.close();
	out.close();
	rename(file, filename);
	FUNCTION_RETURN
}

size_t store::get_retries(std::string const & username) {
	FUNCTION_ENTER
	std::ifstream in(filename, std::ios::in);
	while (in.good()) {
		std::string name, serial;
		int retries;
		in >> name >> serial >> retries;
		if (name == username) {
			in.close();
			FUNCTION_RETURN
			return retries;
		}
	}

	in.close();
	FUNCTION_RETURN
	return 0;
}

void store::reset_retries(std::string const & username) {
	FUNCTION_ENTER
	std::ifstream in(filename, std::ios::in);
	auto file = "/etc/.tokens";//tmpnam(0);
	std::ofstream out(file, std::ios::out);
	while (in.good()) {
		std::string name, serial;
		int retries;
		in >> name >> serial >> retries;

		if (name.length() == 0) {
			continue;
		}

		if (name == username) {
			retries = 0;
		}

		out << name << " " << serial << " " << retries << "\n";
	}

	in.close();
	out.close();
	rename(file, filename);
	FUNCTION_RETURN
}

void store::increment_retries(std::string const & username) {
	FUNCTION_ENTER
	std::ifstream in(filename, std::ios::in);
	auto file = "/etc/.tokens";//tmpnam(0);
	std::ofstream out(file, std::ios::out);
	while (in.good()) {
		std::string name, serial;
		int retries;
		in >> name >> serial >> retries;

		if (name.length() == 0) {
			continue;
		}

		if (name == username) {
			++retries;
		}

		out << name << " " << serial << " " << retries << "\n";
	}

	in.close();
	out.close();
	rename(file, filename);
	FUNCTION_RETURN
}




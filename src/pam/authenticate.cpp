/*
 * authenticate.cpp
 *
 *  Created on: May 8, 2014
 *      Author: Nikolay Filchenko
 */

#define PAM_SM_AUTH
#define PAM_SM_ACCOUNT
#define PAM_SM_SESSION
#define PAM_SM_PASSWORD

#include <syslog.h>
#include <security/pam_ext.h>
#include <security/pam_appl.h>
#include <security/pam_modules.h>

#include "pam/pam.h"
#include "core/log.h"
#include "core/debug.h"

#include <iostream>
#include <cstring>

#include "config.h"
#include "usb/usb.h"

#include "core/aes.h"
#include "core/store.h"
#include "core/key.h"
#include "core/options.h"
#include "pam/pam.h"
#include <limits>

PAM_EXTERN int pam_sm_authenticate(pam_handle_t *pamh, int flags, int argc,
		const char **argv) {
	pam_guard guard(pamh);
	FUNCTION_ENTER
	auto opt = options(argc, argv);

	auto user = pam_context()->user();
	auto serial = store::instance().get_serial(user);

	if (serial.length() == 0) {
		FUNCTION_RETURN
		return PAM_AUTH_ERR;
	}

	auto retry_limit = opt.get("num_retries", std::numeric_limits<size_t>::max());

	if (retry_limit != std::numeric_limits<size_t>::max()) {
		auto retires = store::instance().get_retries(user);
		if (retires >= retry_limit) {
			logger::error << "Account " << user << " locked";
			FUNCTION_RETURN
			return PAM_MAXTRIES;
		}
	}

	usb usb_;

	std::string key = "";
	for (auto tok : usb_.tokens()) {
		if (tok.get_serial() == serial) {
			key = tok.read_key();
		}
	}

	if (key.length() == 0) {
		logger::error << "No matching USB key found";
		FUNCTION_RETURN
		return PAM_AUTHINFO_UNAVAIL;
	}

	auto passphrase = pam_context()->ask("Passphrase:", true);

	if (passphrase.length() == 0) {
		FUNCTION_RETURN
		return PAM_AUTHINFO_UNAVAIL;
	}

	auto token = aes(passphrase).decrypt(key);

	if (token.length() == 0) {
		logger::error << "Wrong passphrase user=" << user << " token=" << serial;
		store::instance().increment_retries(user);
		FUNCTION_RETURN
		return PAM_AUTH_ERR;
	}

	if (opt.has_flag("reset_counter")) {
		store::instance().reset_retries(user);
	}

	pam_context()->set_item(PAM_AUTHTOK, token);

	if (opt.has_flag("reset_key")) {
		pam_context()->set_item(PAM_OLDAUTHTOK, passphrase);
	}

	FUNCTION_RETURN
	return PAM_SUCCESS;
}


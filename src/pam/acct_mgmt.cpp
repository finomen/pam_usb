/*
 * acct_mgmt.cpp
 *
 *  Created on: Jun 12, 2014
 *      Author: Nikolay Filchenko
 */

#define PAM_SM_AUTH
#define PAM_SM_ACCOUNT
#define PAM_SM_SESSION
#define PAM_SM_PASSWORD

#include <syslog.h>
#include <security/pam_ext.h>
#include <security/pam_appl.h>
#include <security/pam_modules.h>

#include "pam/pam.h"
#include "core/log.h"
#include "core/debug.h"

#include <iostream>
#include <cstring>

#include "config.h"
#include "usb/usb.h"

#include "core/aes.h"
#include "core/store.h"
#include "core/key.h"
#include "core/options.h"
#include "pam/pam.h"
#include <limits>

PAM_EXTERN int pam_sm_acct_mgmt(pam_handle_t *pamh, int flags, int argc,
		const char **argv) {
	pam_guard guard(pamh);
	FUNCTION_ENTER
	auto opt = options(argc, argv);


	if (opt.has_flag("reset_key")) {
		std::cout << "Key update required" << std::endl;
		FUNCTION_RETURN
		return PAM_NEW_AUTHTOK_REQD;
	}


	FUNCTION_RETURN
	return PAM_SUCCESS;
}

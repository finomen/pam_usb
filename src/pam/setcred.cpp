/*
 * setcred.cpp
 *
 *  Created on: Jun 12, 2014
 *      Author: Nikolay Filchenko
 */


#define PAM_SM_AUTH
#define PAM_SM_ACCOUNT
#define PAM_SM_SESSION
#define PAM_SM_PASSWORD

#include <syslog.h>
#include <security/pam_ext.h>
#include <security/pam_appl.h>
#include <security/pam_modules.h>

#include "pam/pam.h"
#include "core/log.h"
#include "core/debug.h"

PAM_EXTERN int pam_sm_setcred(pam_handle_t * pamh, int flags, int argc,
		const char **argv) {
	pam_guard guard(pamh);
	FUNCTION_ENTER

	FUNCTION_RETURN
	return PAM_SUCCESS;
}


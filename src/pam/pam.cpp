/*
 * pam.cpp
 *
 *  Created on: May 8, 2014
 *      Author: finomen
 */

#include <syslog.h>
#include <security/pam_ext.h>
#include <security/pam_appl.h>
#include <security/pam_modules.h>
#include <cstring>

#include "pam/pam.h"
#include "core/log.h"
#include "core/debug.h"

pam * pam_context(pam_handle * ph, bool remove) {
	static pam * pm = 0;
	if (ph != 0) {
		pm = new pam(ph);
	} else if (pm && remove) {
		delete pm;
	}

	return pm;
}

pam::pam(pam_handle * ph) : ph(ph){
}

pam::~pam() {
}

std::string pam::user() const {
	FUNCTION_ENTER
	const char * user;
	auto rv = pam_get_user(ph, &user, 0);

	if (rv != PAM_SUCCESS) {
		logger::error << "pam_get_user() failed " << pam_strerror(ph, rv);
		FUNCTION_RETURN
		return "";
	}

	FUNCTION_RETURN
	return user;
}

std::string pam::get_item(int id) const {
	FUNCTION_ENTER
	const void * value;
	auto rv = pam_get_item(ph, id, &value);

	if (rv != PAM_SUCCESS) {
		logger::error << "pam_get_user() failed " << pam_strerror(ph, rv);
		FUNCTION_RETURN
		return "";
	}

	FUNCTION_RETURN
	return value ? reinterpret_cast<const char *>(value) : "";
}

void pam::set_item(int id, std::string const & value) const {
	FUNCTION_ENTER
	pam_set_item(ph, id, value.c_str());
	FUNCTION_RETURN
}

std::string pam::ask(std::string const & what, bool hide_input) {
	int rv;
	pam_message msg;
	msg.msg_style = hide_input ? PAM_PROMPT_ECHO_OFF : PAM_PROMPT_ECHO_ON;
	msg.msg = what.c_str();
	pam_conv *conv;

	rv = pam_get_item(ph, PAM_CONV, (const void **) &conv);
	if (rv != PAM_SUCCESS) {
		return "";
	}

	if ((conv == NULL) || (conv->conv == NULL)) {
		return "";
	}

	pam_message *msgp[] = { &msg };
	pam_response *resp;
	rv = conv->conv(1, (const pam_message **) msgp, &resp, conv->appdata_ptr);

	if (rv != PAM_SUCCESS) {
		return "";
	}

	if ((resp == NULL) || (resp[0].resp == NULL)) {
		return "";
	}

	auto res = std::string(resp[0].resp);
	memset(resp[0].resp, 0, strlen(resp[0].resp));
	free(&resp[0]);

	return res;
}

pam::operator pam::handle() const {
	return ph;
}

pam_guard::pam_guard(pam_handle * ph) {
	pam_context(ph);
}

pam_guard::~pam_guard() {
	pam_context(0);
}

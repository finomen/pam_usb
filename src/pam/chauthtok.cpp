/*
 * chauthtok.cpp
 *
 *  Created on: Jun 11, 2014
 *      Author: Nikolay Filchenko
 */

#define PAM_SM_AUTH
#define PAM_SM_ACCOUNT
#define PAM_SM_SESSION
#define PAM_SM_PASSWORD

#include <syslog.h>
#include <security/pam_ext.h>
#include <security/pam_appl.h>
#include <security/pam_modules.h>
#include <security/pam_modutil.h>

#include "pam/pam.h"
#include "core/log.h"
#include "core/debug.h"
#include "core/store.h"
#include "core/key.h"
#include "core/aes.h"
#include "usb/usb.h"
#include "core/options.h"

#include <iostream>
#include <cstring>
#include <cstdlib>
#include "config.h"
#include <unistd.h>

PAM_EXTERN int pam_sm_chauthtok(pam_handle_t *pamh, int flags, int argc, const char **argv) {
	pam_guard guard(pamh);
	FUNCTION_ENTER
	auto opt = options(argc, argv);

	auto do_update = (flags & PAM_UPDATE_AUTHTOK) == PAM_UPDATE_AUTHTOK;
	auto user = pam_context()->user();

	usb usb_;

	auto uid = getuid();

	if (uid != 0 && !do_update) {
		auto serial = store::instance().get_serial(user);

		if (serial.length() == 0) {
			FUNCTION_RETURN
			return PAM_AUTH_ERR;
		}

		std::string key = "";
		for (auto tok : usb_.tokens()) {
			if (tok.get_serial() == serial) {
				key = tok.read_key();
			}
		}

		if (key.length() == 0) {
			FUNCTION_RETURN
			return PAM_TRY_AGAIN;
		}

		auto passphrase = std::string("");//pam_context()->get_item(PAM_OLDAUTHTOK);

		if (passphrase.length() == 0) {
			passphrase = pam_context()->ask("Passphrase: ", true);
		}

		auto pass = aes(passphrase).decrypt(key);

		if (pass.length() == 0) {
			FUNCTION_RETURN
			return PAM_AUTH_ERR;
		}

		pam_context()->set_item(PAM_OLDAUTHTOK, pass);
	}

	if (do_update) {
		auto new_pass = gen_key(opt.has_flag("strong_random"));

		auto passphrase = pam_context()->ask("New passphrase: ", true);
		auto passphrase_c = pam_context()->ask("Confirm: ", true);

		if (passphrase.length() == 0 || passphrase != passphrase_c) {
			FUNCTION_RETURN
			return PAM_TRY_AGAIN;
		}

		auto new_key = aes(passphrase).encrypt(new_pass);
		if (aes(passphrase).decrypt(new_key) != new_pass) {
			FUNCTION_RETURN
			return PAM_TRY_AGAIN;
		}

		auto tokens = usb_.tokens();

		if (tokens.size() == 0) {
			FUNCTION_RETURN
			return PAM_TRY_AGAIN;
		}

		for (size_t i = 1; i <= tokens.size(); ++i) {
			std::cout << i << " = " << tokens[i - 1].get_name() << std::endl;
		}

		size_t selected = 0;

		while (selected <= 0 || selected > tokens.size()) {
			auto answer = pam_context()->ask("Token: ");
			if (answer.length() == 0) {
				FUNCTION_RETURN
				return PAM_TRY_AGAIN;
			}

			selected = atoi(answer.c_str());
		}

		--selected;

		auto new_serial = tokens[selected].get_serial();
		if (!tokens[selected].write_key(new_key)) {
			FUNCTION_RETURN
			return PAM_AUTHTOK_ERR;
		}

		store::instance().set_serial(user, new_serial);
		store::instance().reset_retries(user);
		pam_context()->set_item(PAM_AUTHTOK, new_pass);
	}

	FUNCTION_RETURN
	return PAM_SUCCESS;
}

